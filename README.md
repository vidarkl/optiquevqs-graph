# OptiqueVQS Graph
Java library to create, load and use graph structures (queries and navigation graphs) in [OptiqueVQS](https://gitlab.com/ernesto.jimenez.ruiz/OptiqueVQS) (1).

## How to use
Either load the library directly into your own project, or run either of the two provided examples.

To run the example in Example1.java, do (requries Maven):

```
mvn clean install
mvn exec:java@Example1
```

To run the example in Example2.java, do

```
mvn clean install
mvn exec:java@Example2
```

## References
1. Klungre, V.N., 2020. **Adaptive Query Extension Suggestions for Ontology-
Based Visual Query Systems.** Ph.D. thesis. University of Oslo, 2020, [link](https://www.duo.uio.no/handle/10852/80614)
2. Ahmet Soylu, Evgeny Kharlamov, Dimitry Zheleznyakov, Ernesto Jimenez Ruiz, Martin Giese, Martin G. Skjaeveland, Dag Hovland, Rudolf Schlatte, Sebastian Brandt, Hallstein Lie, Ian Horrocks. **OptiqueVQS: a Visual Query System over Ontologies for Industry**. Semantic Web Journal, 2018. [link](http://semantic-web-journal.net/content/optiquevqs-visual-query-system-over-ontologies-industry-0)
3. Klungre, Ahmet Soylu, Ernesto Jimenez-Ruiz, Evgeny Kharlamov, Martin Giese. **Query extension suggestions for visual query systems through ontology projection and indexing**. New Generation Computing, 2019 [link](https://link.springer.com/article/10.1007/s00354-019-00071-1)
4. S. Malyshev, M. Krötzsch, L. González, J. Gonsior and A. Bielefeldt. **Getting the Most out of Wikidata: Semantic Technology Us- age in Wikipedia’s Knowledge Graph**. Proceedings of the 17th International Semantic Web Conference (ISWC’18), D. Vrandecic, K. Bontcheva, M.C. Suárez-Figueroa, V. Presutti, I. Celino, M. Sabou, L.-A. Kaffee and E. Simperl, eds, LNCS, Vol. 11137, Springer, 2018, pp. 376–394.

