package eu.optiquevqs.graph;

import eu.optiquevqs.graph.navigation.*;
import eu.optiquevqs.graph.query.*;


// A toy example showing how to make naviation graphs
public class Example1 {

    public static void main(String[] args) throws Exception {

        // CONSTRUCT NAVIGATION GRAPH
        // Construct a new navigation graph
        NavigationGraph ng = new NavigationGraph();

        // Creating classes
        ClassNode personClass = new ClassNode("Person");
        ClassNode carClass = new ClassNode("Car");
        ClassNode petClass = new ClassNode("Pet");

        // Creating datatypes
        DatatypeNode stringDatatype = new DatatypeNode("String");
        DatatypeNode integerDatatype = new DatatypeNode("Integer");

        // Add classes
        ng.addVertex(personClass);
        ng.addVertex(carClass);
        ng.addVertex(petClass);

        // Add datatypes
        ng.addVertex(stringDatatype);
        ng.addVertex(integerDatatype);

        // Add edges to the navigation graph
        ng.addEdge(personClass, carClass, new PropertyEdge("owns"));
        ng.addEdge(personClass, petClass, new PropertyEdge("owns"));
        ng.addEdge(personClass, integerDatatype, new PropertyEdge("age"));
        ng.addEdge(personClass, stringDatatype, new PropertyEdge("name"));
        ng.addEdge(petClass, stringDatatype, new PropertyEdge("name"));

        // Print the navigation graph
        System.out.println("Navigation graph:");
        System.out.println(ng);







        // MAKE A QUERY
        // Construct query. Connect it to the navigation graph constructed above.
        QueryGraph q1 = new QueryGraph(ng);
        
        // System.out.println(q1.getNavigationGraph());
        QueryVariable v1 = new QueryVariable("v1", "Person");
        QueryVariable v2 = new QueryVariable("v2", "Car");
        q1.addVertex(v1);
        q1.addVertex(v2);
        q1.addEdge(v1, v2, new PropertyEdge("owns"));
        System.out.println(q1);
        q1.setRoot(v1);
        System.out.println("Root is now " + q1.getRoot());
        q1.setRoot("v2");
        System.out.println("Root is now " + q1.getRoot());

        System.out.println("Is v1 an object variable?");
        System.out.println(q1.isObjectVariable(v1));
        System.out.println("Is v1 a data variable?");
        System.out.println(q1.isDataVariable(v1));

        q1.prettyPrint();



        // How to make a query with QueryGraphFactory
        NavigationGraph ng3 = new NavigationGraph();
        NavigationGraphNode cPerson = new ClassNode("http://www.example.org/example#Person");
        NavigationGraphNode cCar= new ClassNode("http://www.example.org/example#Car");
        NavigationGraphNode cManufacturer = new ClassNode("http://www.example.org/example#Manufacturer");
        NavigationGraphNode cString= new DatatypeNode("http://www.example.org/example#String");
        ng3.addVertex(cPerson);
        ng3.addVertex(cCar);
        ng3.addVertex(cManufacturer);
        ng3.addVertex(cString);
        ng3.addEdge(cPerson, cCar, new PropertyEdge("http://www.example.org/example#owns"));
        ng3.addEdge(cPerson, cPerson, new PropertyEdge("http://www.example.org/example#knows"));
        ng3.addEdge(cCar, cManufacturer, new PropertyEdge("http://www.example.org/example#madeBy"));
        ng3.addEdge(cPerson, cString, new PropertyEdge("http://www.example.org/example#name"));

        String queryString = "SELECT * WHERE {" +
        					 "?p1 a <http://www.example.org/example#Person>;" +
        					 "<http://www.example.org/example#knows> ?p2;" +
        					 "<http://www.example.org/example#knows> ?p3;" +
        					 "<http://www.example.org/example#owns> ?c." +
        					 "?c a <http://www.example.org/example#Car>;" +
        					 "<http://www.example.org/example#madeBy> ?m." +
        					 "?m a <http://www.example.org/example#Manufacturer>." +
        					 "?p2 a <http://www.example.org/example#Person>;" +
        					 "<http://www.example.org/example#name> ?n." +
        					 "?p3 a <http://www.example.org/example#Person>." +
        					 "?n a <http://www.example.org/example#String>." +
        					 "}";

        QueryGraph q3 = QueryGraphFactory.create(queryString, ng3);

        // Get QueryVariable from a label
        QueryVariable v = q3.getVariable("n");

        System.out.println("Is name an object variable?");
        System.out.println(q3.isObjectVariable(v));
        System.out.println("Is name a data variable?");
        System.out.println(q3.isDataVariable(v));


        // How to make a query without navigation graph
        System.out.println("no navigation graph");
        QueryGraph q4 = new QueryGraph();
        System.out.println(q4.getNavigationGraph());
        QueryVariable v41 = new QueryVariable("v1", "Person");
    }

}
