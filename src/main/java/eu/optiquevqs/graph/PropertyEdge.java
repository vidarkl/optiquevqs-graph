package eu.optiquevqs.graph;

import org.jgrapht.graph.DefaultEdge;

// A labeled edge. Used in all graphs to represent properties
public class PropertyEdge extends DefaultEdge {
    private String label;  // The label of the propertyEdge
    private boolean inverted;  // Is this needed? What is it for?

    public PropertyEdge(String label) {
        this.label = label;
        this.inverted = false;
    }

    public PropertyEdge(String label, boolean invertedEdge) {
        this.label = label;
        this.inverted = invertedEdge;
    }

    public String getLabel() {
        return this.label;
    }

    @Override
    public String toString() {
        return "[" + this.label + "]";
    }

    public boolean isInverted() {
    	return this.inverted;
    }
}