package eu.optiquevqs.graph.query;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jgrapht.Graph;
import org.jgrapht.alg.DijkstraShortestPath;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;

import eu.optiquevqs.graph.PropertyEdge;
import eu.optiquevqs.graph.navigation.NavigationGraph;


// This is a special class used to generate statistics over a set of queries, like a query log.
class QueryStats {
	double weight = 0.0;

	int datatypePropertyCount = 0;
	int objectPropertyCount = 0;
	int edgeCount = 0;
	int uniqueDatatypePropertyCount = 0;
	int uniqueObjectPropertyCount = 0;
	int uniqueEdgeCount = 0;
	int literalNodeCount = 0;
	int typedNodeCount = 0;
	int nodeCount = 0;
	int uniqueLiteralNodeCount = 0;
	int uniqueTypedNodeCount = 0;
	int uniqueNodeCount = 0;
	int diameter = 0;
	int maxOutDegree = 0;
	int maxInDegree = 0;
}
/*
 *  Useage: Run QuerylogStats.querylogStatsToFile(querylog, path). 
 *  Args: 
 *  	querylog: Collection of QueryGraph instances 
 *  	path: Relative path for output file
 *  
 *  Note that when counting unique edges, there is not differentiation between different properties with the same name.
 *  E.g. owns(Person, Car) and owns(Company, Property) is considered the same property.
 */
public class QuerylogStats {

	public static void querylogStatsToFile(Collection<QueryGraph> querylog, String path) {
		FileWriter fileWriter = null;
		try {
			fileWriter = new FileWriter(path + "query_stats.csv");
			writeHeader(fileWriter);
			for(QueryGraph query : querylog) {
				QueryStats queryStats = calculateQueryStats(query);
				writeQuery(fileWriter, queryStats);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				fileWriter.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private static QueryStats calculateQueryStats(QueryGraph query) {
		QueryStats stats = new QueryStats();
		weight(query, stats);
		propertyCount(query, stats);
		nodeCount(query, stats);
		diameter(query, stats);
		maxDegreeCount(query, stats);
		return stats;
	}

	private static void weight(QueryGraph query, QueryStats stats) {
		stats.weight = query.getWeight();
	}

	// Does not differentiate between different properties with the same name.
	// E.g. owns(Person, Car) and owns(Company, Property) is considered the same property.
	private static void propertyCount(QueryGraph query, QueryStats stats) {
		NavigationGraph ng = query.getNavigationGraph();
		Set<PropertyEdge> edges = query.edgeSet();
		Set<String> seenEdges = new HashSet<>();

		for(PropertyEdge edge : edges) {
			String edgeLabel = edge.getLabel();
			boolean edgeLabelSeen = seenEdges.contains(edgeLabel);
			seenEdges.add(edgeLabel);

			if (ng.isDatatypeProperty(edgeLabel)) {
				stats.datatypePropertyCount++;
				if(!edgeLabelSeen) stats.uniqueDatatypePropertyCount++;
			} else {
				stats.objectPropertyCount++;
				if(!edgeLabelSeen) stats.uniqueObjectPropertyCount++;
			}
			stats.edgeCount++;
			if(!edgeLabelSeen) stats.uniqueEdgeCount++;
		}
	}

	private static void nodeCount(QueryGraph query, QueryStats stats) {
		NavigationGraph ng = query.getNavigationGraph();
		Set<String> classes = ng.getAllClasses();
		Set<QueryVariable> nodes = query.vertexSet();
		Set<String> seenTypes = new HashSet<>();

		for(QueryVariable node : nodes) {
			String nodeType = node.getType();
			boolean nodeTypeSeen = seenTypes.contains(nodeType);
			seenTypes.add(nodeType);

			if (classes.contains(nodeType)) {
				stats.typedNodeCount++;
				if(!nodeTypeSeen) stats.uniqueTypedNodeCount++;
			} else {
				stats.literalNodeCount++;
				if(!nodeTypeSeen) stats.uniqueLiteralNodeCount++;
			}
			stats.nodeCount++;
			if(!nodeTypeSeen) stats.uniqueNodeCount++;
		}
	}

	private static void maxDegreeCount(QueryGraph query, QueryStats stats) {
		Set<QueryVariable> nodes = query.vertexSet();
		int maxInDegree = 0;
		int maxOutDegree = 0;

		for(QueryVariable node : nodes) {
			int inDegree = query.inDegreeOf(node);
			int outDegree = query.outDegreeOf(node);
			if(inDegree > maxInDegree) maxInDegree = inDegree;
			if(outDegree > maxOutDegree) maxOutDegree = outDegree;
		}

		stats.maxInDegree = maxInDegree;
		stats.maxOutDegree = maxOutDegree;
	}

	private static void diameter(QueryGraph query, QueryStats stats) {
		Graph<QueryVariable, DefaultEdge> undirectedQuery = createUndirectedGraph(query);
		Set<QueryVariable> nodeSet = undirectedQuery.vertexSet();
		List<QueryVariable> nodeList = new ArrayList<>(nodeSet);
		int pathMaxLength = 0;

		for(int i = 0; i < nodeList.size(); i++) {
			for(int j = 0; j < nodeList.size(); j++) {
				QueryVariable node1 = nodeList.get(i);
				QueryVariable node2 = nodeList.get(j);
				DijkstraShortestPath<QueryVariable, DefaultEdge> shortestPath =
						new DijkstraShortestPath<>(undirectedQuery, node1, node2);
				double pathLengthDouble = shortestPath.getPathLength();
				if(Double.isInfinite(pathLengthDouble)) continue;
				int pathLengthInt = (int)pathLengthDouble;
				if(pathLengthInt > pathMaxLength) pathMaxLength = pathLengthInt;
			}
		}

		stats.diameter = pathMaxLength;
	}

	// Help method for diameter()
	private static Graph<QueryVariable, DefaultEdge> createUndirectedGraph(QueryGraph query) {
		Graph<QueryVariable, DefaultEdge> undirectedQuery = new SimpleGraph<>(DefaultEdge.class);

		Map<String, QueryVariable> newNodes = new HashMap<>();

		Set<QueryVariable> oldNodes = query.vertexSet();
		for(QueryVariable oldNode : oldNodes) {
			String variableName = oldNode.getLabel();
			QueryVariable newNode = new QueryVariable(variableName, "");
			undirectedQuery.addVertex(newNode);
			newNodes.put(variableName, newNode);
		}

		Set<PropertyEdge> oldEdges = query.edgeSet();
		for(PropertyEdge oldEdge : oldEdges) {
			String sourceName = query.getEdgeSource(oldEdge).getLabel();
			String targetName = query.getEdgeTarget(oldEdge).getLabel();
			undirectedQuery.addEdge(newNodes.get(sourceName), newNodes.get(targetName), new DefaultEdge());
		}

		return undirectedQuery;
	}

	private static void writeHeader(FileWriter fileWriter) {
		List<String> columns = Arrays.asList(
			"weight",
			"datatypePropertyCount",
			"objectPropertyCount",
			"edgeCount",
			"uniqueDatatypePropertyCount",
			"uniqueObjectPropertyCount",
			"uniqueEdgeCount",
			"literalNodeCount",
			"typedNodeCount",
			"nodeCount",
			"uniqueLiteralNodeCount",
			"uniqueTypedNodeCount",
			"uniqueNodeCount",
			"diameter",
			"maxOutDegree",
			"maxInDegree"
		);

		String header = String.join(",", columns);

		try {
			fileWriter.write(header + "\n");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void writeQuery(FileWriter fileWriter, QueryStats queryStats) {
		List<String> columns = Arrays.asList(
			String.valueOf(queryStats.weight),
			String.valueOf(queryStats.datatypePropertyCount),
			String.valueOf(queryStats.objectPropertyCount),
			String.valueOf(queryStats.edgeCount),
			String.valueOf(queryStats.uniqueDatatypePropertyCount),
			String.valueOf(queryStats.uniqueObjectPropertyCount),
			String.valueOf(queryStats.uniqueEdgeCount),
			String.valueOf(queryStats.literalNodeCount),
			String.valueOf(queryStats.typedNodeCount),
			String.valueOf(queryStats.nodeCount),
			String.valueOf(queryStats.uniqueLiteralNodeCount),
			String.valueOf(queryStats.uniqueTypedNodeCount),
			String.valueOf(queryStats.uniqueNodeCount),
			String.valueOf(queryStats.diameter),
			String.valueOf(queryStats.maxOutDegree),
			String.valueOf(queryStats.maxInDegree)
		);

		String stats = String.join(",", columns);

		try {
			fileWriter.write(stats + "\n");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
