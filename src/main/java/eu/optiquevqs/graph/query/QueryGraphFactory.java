package eu.optiquevqs.graph.query;

import eu.optiquevqs.graph.PropertyEdge;
import eu.optiquevqs.graph.navigation.NavigationGraph;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import org.apache.jena.graph.Node;
import org.apache.jena.graph.Triple;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.sparql.algebra.Algebra;
import org.apache.jena.sparql.algebra.Op;
import org.apache.jena.sparql.algebra.op.OpBGP;
import org.apache.jena.sparql.algebra.op.OpFilter;
import org.apache.jena.sparql.algebra.op.OpPath;
import org.apache.jena.sparql.algebra.op.OpSequence;
import org.apache.jena.sparql.algebra.optimize.TransformPathFlattern;
import org.apache.jena.sparql.core.BasicPattern;
import org.apache.jena.sparql.core.Var;

public class QueryGraphFactory {

	final static String[] TYPE_URIS = new String[] {
		"http://www.w3.org/1999/02/22-rdf-syntax-ns#type",
		"http://www.wikidata.org/prop/direct/P31"	
	};

	/*
	 *  Create a Set of QueryGraphs from a file containing multiple queries.
	 *  File must be tsv with weights (WEIGHT QUERY).
	 *  
	 *  Only queries that are Optique VQS compatible will be returned. 
	 */
	public static Set<QueryGraph> readOptiqueQueries(String filename, NavigationGraph navigationGraph) {
		Set<QueryGraph> queries = new HashSet<>();

		Scanner fileReader = null;
		try {
			fileReader = new Scanner(new File(filename));
		} catch (FileNotFoundException e) {
			System.out.println(e);
		}

		while(fileReader.hasNext()) {
			String line = fileReader.nextLine();
			String[] linesplit = line.split("\t", -1);
			double weight = Double.parseDouble(linesplit[0]);
			QueryGraph qg = QueryGraphFactory.create(linesplit[1], navigationGraph);
			if(!qg.isOptiqueCompatible()) continue;
			qg.setWeight(weight);
			queries.add(qg);
		}

		return queries; 
	}

	/*
	 *  Create a QueryGraph from file. File must contain a single
	 *  query and cannot contain projection variables. Must start
	 *  with "SELECT * WHERE...".
	 */
	public static QueryGraph read(String filename, NavigationGraph navigationGraph) {
		Query q = QueryFactory.read(filename);
		BasicPattern p = queryToBasicPattern(q);
		return basicPatternToQueryGraph(p, navigationGraph);
	}

	/*
	 *  Create a QueryGraph from a SPARQL query string.
	 */
	public static QueryGraph create(String queryString, NavigationGraph navigationGraph) {
		Query q = QueryFactory.create(queryString);
		BasicPattern p = queryToBasicPattern(q);
		return basicPatternToQueryGraph(p, navigationGraph);
	}

	private static BasicPattern queryToBasicPattern(Query query) {
		Op op = Algebra.compile(query);
		if (op instanceof OpFilter) op = ((OpFilter)op).getSubOp();
		if (op instanceof OpSequence) return opSequenceToBasicPattern((OpSequence)op);
		OpBGP opBGP = (OpBGP)op;
		return opBGP.getPattern();
	}

	// For queries containing inverse properties (?x ^:prop ?y).
	private static BasicPattern opSequenceToBasicPattern(OpSequence op) {
		BasicPattern pattern = new BasicPattern();
		Iterator<Op> it = ((OpSequence)op).iterator();
		while(it.hasNext()) {
			Op current = it.next();
			if(current instanceof OpPath) {
				current = ((OpPath)current).apply(new TransformPathFlattern());
			}
			BasicPattern subPattern = ((OpBGP)current).getPattern();
			Triple subTriple = subPattern.get(0);
			pattern.add(subTriple);
		}
		return pattern;
	}

	private static QueryGraph basicPatternToQueryGraph(BasicPattern pattern, NavigationGraph navigationGraph) {
		QueryGraph qg = new QueryGraph(navigationGraph);

		Map<Node, QueryVariable> objectVariables = getObjectVariables(pattern);
		setObjectVariables(objectVariables, qg);
		setObjectProperties(pattern, objectVariables, qg);

		Map<Node, QueryVariable> dataVariables = getDataVariables(pattern, objectVariables, navigationGraph);
		setDataProperties(pattern, objectVariables, dataVariables, qg);

		return qg;
	}

	private static boolean isTypePredicate(Node predicate) {
		return Arrays.asList(TYPE_URIS).contains(predicate.getURI());
	}

	// Get all object variables from a pattern. I.e. variables that have a type.
	private static Map<Node, QueryVariable> getObjectVariables(BasicPattern pattern) {
		Map<Node, QueryVariable> objectVariables = new HashMap<Node, QueryVariable>();
		for (Triple triple : pattern) {
			Node subject = triple.getSubject();
			Node predicate = triple.getPredicate();
			Node object = triple.getObject();

			if (isTypePredicate(predicate)) {
				objectVariables.put(subject, new QueryVariable(subject.getName(), object.getURI()));
			}
		}
		return objectVariables;
	}

	private static void setObjectVariables(Map<Node, QueryVariable> objectVariables, QueryGraph qg) {
		for (QueryVariable var : objectVariables.values()) qg.addVertex(var);
	}

	private static void setObjectProperties(BasicPattern pattern,
			Map<Node, QueryVariable> objectVariables,
			QueryGraph qg) {

		for (Triple triple : pattern) {
			Node subject = triple.getSubject();
			Node predicate = triple.getPredicate();
			Node object = triple.getObject();

			if(!isTypePredicate(predicate) && objectVariables.containsKey(object)) {
				QueryVariable s = objectVariables.get(subject);
				QueryVariable o = objectVariables.get(object);
				PropertyEdge p = new PropertyEdge(predicate.getURI());
				qg.addEdge(s, o, p);
			}
		}
	}

	// Get data variables from a pattern. I.e. variables that have a datatype.
	private static Map<Node, QueryVariable> getDataVariables(BasicPattern pattern,
			Map<Node, QueryVariable> objectVariables,
			NavigationGraph navigationGraph) {
		Map<Node, QueryVariable> dataVariables = new HashMap<Node, QueryVariable>();

		for (Triple triple : pattern) {
			Node subject = triple.getSubject();
			Node predicate = triple.getPredicate();
			Node object = triple.getObject();

			if(!isTypePredicate(predicate) && !objectVariables.containsKey(object) && object instanceof Var) {
				String s = objectVariables.get(subject).getType();
				String p = predicate.getURI();
				String dataType = navigationGraph.getDatatype(s, p);
				dataVariables.put(object, new QueryVariable(object.getName(), dataType));
			}
		}

		return dataVariables;
	}

	private static void setDataProperties(BasicPattern pattern,
			Map<Node, QueryVariable> objectVariables,
			Map<Node, QueryVariable> dataVariables,
			QueryGraph qg) {
		for (Triple triple : pattern) {
			Node subject = triple.getSubject();
			Node predicate = triple.getPredicate();
			Node object = triple.getObject();

			if(!isTypePredicate(predicate) && dataVariables.containsKey(object)) {
				QueryVariable s = objectVariables.get(subject);
				QueryVariable o = dataVariables.get(object);
				PropertyEdge p = new PropertyEdge(predicate.getURI());
				qg.addVertex(s);
				qg.addVertex(o);
				qg.addEdge(s, o, p);
			}
		}
	}



}
