package eu.optiquevqs.graph.query;

import eu.optiquevqs.graph.*;
import eu.optiquevqs.graph.navigation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jgrapht.*;
import org.jgrapht.alg.ConnectivityInspector;
import org.jgrapht.alg.CycleDetector;
import org.jgrapht.alg.DijkstraShortestPath;
import org.jgrapht.graph.*;

// A query is a labeled multigraph. For OptiqueVQS, this is almost always a tree.
public class QueryGraph extends DirectedMultigraph<QueryVariable, PropertyEdge>{

    private NavigationGraph navigationGraph;  // A reference to the navigation graph this query is built over.
    private QueryVariable root;  // The root or focus variable of the query.
    private double weight;  // If multiple queries are used, weights are used to indicate its importance.

    // Constructor without navigation graph
    public QueryGraph(){
        super(PropertyEdge.class);
    }

    // Constructor where the navigation graph is given directly.
    public QueryGraph(NavigationGraph navigationGraph){
        super(PropertyEdge.class);
        this.navigationGraph = navigationGraph;
    }

    // Copy constructor
    public QueryGraph(QueryGraph another) {
        this(another.navigationGraph);
        this.root = another.getRoot();
        this.weight = another.getWeight();

        Set<QueryVariable> vars = another.vertexSet();
        for(QueryVariable var : vars) {
            this.addVertex(new QueryVariable(var.getLabel(), var.getType()));
        }
        Set<PropertyEdge> edges = another.edgeSet();
        for(PropertyEdge edge : edges) {
            QueryVariable source = null;
            QueryVariable target = null;
            for (QueryVariable var : vars) {
                if (var.equals(another.getEdgeSource(edge))) {
                    source = var;
                } else if (var.equals(another.getEdgeTarget(edge))) {
                    target = var;
                }
            }
            this.addEdge(source, target, new PropertyEdge(edge.getLabel()));
        }
    }

    // Get the navigation graph the query is over.
    public NavigationGraph getNavigationGraph() {
        return this.navigationGraph;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getWeight() {
        return this.weight;
    }

    // Set root by giving a vertex.
    public void setRoot(QueryVariable root) {
        this.root = root;
    }

    // Set root using the variable name.
    public void setRoot(String rootVariableName) {
        this.root = root;
    }

    public QueryVariable getRoot() {
        return this.root;
    }

    // Given a variable label, return the corresponding QueryVariable
    // Return null if such a variable does not exist.
    public QueryVariable getVariable(String variableName) {
        for(QueryVariable qv : this.vertexSet()) {
            if (qv.getLabel().equals(variableName)) {
                return qv;
            }
        }
        return null;
    }

    // Check if query contains a variable typed to the given class
    public boolean containsClass(String className) {
        for(QueryVariable qv : this.vertexSet()) {
            if (qv.getType().equals(className)) {
                if (this.navigationGraph.getAllClasses().contains(className)) return true;
                break;
            }
        }
        return false;
    }

    // Help method for detectPseudoCycles(). DFS search in undirected graph.
    private boolean detectUndirectedCycles(AsUndirectedGraph<QueryVariable, PropertyEdge> ug,
            QueryVariable node,
            QueryVariable parent,
            Set<QueryVariable> visited) {
        visited.add(node);
        Set<PropertyEdge> outgoingEdges = ug.edgesOf(node);
        for (PropertyEdge e : outgoingEdges) {
            QueryVariable target = null;
            target = (ug.getEdgeTarget(e) == node) ? ug.getEdgeSource(e) : ug.getEdgeTarget(e);
            if (!visited.contains(target)) {
                if (detectUndirectedCycles(ug, target, node, visited)) return true;
            } else if (target != parent) {
                return true;
            }
        }
        return false;
    }

    // Checks if this query is optique compatible.
    public boolean isOptiqueCompatible() {
        return !this.detectPseudoCycles() && this.isConnected() && !this.literalVariableWithMultipleInEdges();
    }

    // Detect circular patterns that are not cycles in directed graphs, but would have been
    // in an undirected graph. E.g. a graph with nodes 1,2,3 and edges: (1,2) (1,3) (2,3).
    private boolean detectPseudoCycles() {
        AsUndirectedGraph<QueryVariable, PropertyEdge> ug = new AsUndirectedGraph<QueryVariable, PropertyEdge>(this);
        Set<QueryVariable> vertexSet = ug.vertexSet();
        QueryVariable startNode = vertexSet.iterator().next();
        return this.detectUndirectedCycles(ug, startNode, null, new HashSet<QueryVariable> ());
    }

    // Check if query graph is weakly connected.
    private boolean isConnected() {
        ConnectivityInspector<QueryVariable, PropertyEdge> inspector = new ConnectivityInspector<QueryVariable, PropertyEdge>(this);
        return inspector.isGraphConnected();
    }

    // Checks if any literal variables in the query have multiple in edges.
    private boolean literalVariableWithMultipleInEdges() {
        Set<String> datatypes = this.navigationGraph.getAllDatatypes();
        for(QueryVariable var : this.vertexSet()) {
            if(datatypes.contains(var.getType()) && this.inDegreeOf(var) > 1) return true;
        }
        return false;
    }

    /*
     * Removes all nodes and edges in one direction from cutEdge. Direction is given by cutNode.
     * cutNode must be source or target for cutEdge.
     * Removal includes cutEdge and cutNode.
     */
    public void removeSubGraph(PropertyEdge cutEdge, QueryVariable cutNode) {
        if(!this.edgesOf(cutNode).contains(cutEdge)) {
            throw new IllegalArgumentException("cutEdge and cutNode must be connected.");
        }
        Set<QueryVariable> nodesToBeDeleted = this.dfsRemoval(cutEdge, cutNode);
        for(QueryVariable node : nodesToBeDeleted) {
            this.removeVertex(node);
        }
    }

    /*
     * Help method for removeSubGraph().
     * Removes edges during traversal. 
     * Returns a set of all nodes to be deleted.
     */
    private Set<QueryVariable> dfsRemoval(PropertyEdge parent, QueryVariable currentNode) {
        Set<QueryVariable> nodesToBeDeleted = new HashSet<>();
        nodesToBeDeleted.add(currentNode);
        this.removeEdge(parent);
        Set<PropertyEdge> edges = this.edgesOf(currentNode);
        for(PropertyEdge edge : edges) {
            boolean currentIsTarget = this.getEdgeTarget(edge).equals(currentNode);
            QueryVariable nextNode = currentIsTarget ? this.getEdgeSource(edge) : this.getEdgeTarget(edge);
            nodesToBeDeleted.addAll(this.dfsRemoval(edge, nextNode));
        }
        return nodesToBeDeleted;
    }

    public boolean isLeaf(QueryVariable var) {
        int degree = this.inDegreeOf(var) + this.outDegreeOf(var);
        return degree == 1;
    }


    // Return true if the variable corresponds to a class in the navigation graph.
    // I.e check if the type of the variable refers to a ClassNode in the navigation graph.
    public boolean isObjectVariable(QueryVariable var) {
        Set <String> allClassLabels = this.navigationGraph.getAllClasses();
        if (allClassLabels.contains(var.getType())) return true;
        return false;
    }

    // Return true if the variable corresponds to a class in the navigation graph.
    // I.e check if the type of the variable refers to a ClassNode in the navigation graph.
    public boolean isDataVariable(QueryVariable var) {
        return !isObjectVariable(var);
    }

    // Just print a pretty version of the query
    public void prettyPrint() {
        String printString = "Root: " + this.root + "\n";
        for (QueryVariable qv : this.vertexSet()) {
            if (isObjectVariable(qv)) printString += "(O) ";
            else  printString += "(D) ";
            printString += qv.getLabel() + ": " + qv.getType() + "\n";
        }
        for (PropertyEdge pe : this.edgeSet()) {
            if (isObjectVariable(this.getEdgeTarget(pe))) printString += "(OP) ";
            else  printString += "(DP) ";
            printString += pe.getLabel() + ": " + this.getEdgeSource(pe).getLabel() + " -> " + this.getEdgeTarget(pe).getLabel()  +  "\n";
        }
        System.out.println(printString);
    }

    // Set of non-literal variables
    public Set<QueryVariable> classNodeSet() {
        Set<QueryVariable> allNodes = this.vertexSet();
        Set<String> classes = this.navigationGraph.getAllClasses();
        Set<QueryVariable> classNodes = new HashSet<>();
        for(QueryVariable var : allNodes) {
            if(classes.contains(var.getType())) classNodes.add(var);
        }
        return classNodes;
    }

    public String toPrettyString() {
        Set<QueryVariable> variables = this.vertexSet();
        String nodes = "[";
        String triples = "[";
        for(QueryVariable source : variables) {
            String sourceLabel = source.getLabel();
            String sourceType = source.getType();
            nodes += sourceLabel + " - " + sourceType + ", ";
            Set<PropertyEdge> outgoingEdges = this.outgoingEdgesOf(source);
            for(PropertyEdge edge : outgoingEdges) {
                String edgeLabel = edge.getLabel();
                QueryVariable target = this.getEdgeTarget(edge);
                String targetLabel = target.getLabel();
                triples += "<" + sourceLabel + ", " + edgeLabel + ", " + targetLabel + ">, ";
            }
        }
        nodes = nodes.substring(0, nodes.length() - 2) + "]  ";
        if(triples.length() > 1) {
            triples = triples.substring(0, triples.length() - 2) + "]";
        } else {
            triples += "]";
        }
        return nodes + triples;
    }

    public int edgeCount() {
        return this.edgeSet().size();
    }

    // Properties with the same name counts as one unique edge
    public int uniqueEdgeCount() {
        Set<String> uniqueEdges = new HashSet<>();
        for(PropertyEdge edge : this.edgeSet()) {
            uniqueEdges.add(edge.getLabel());
        }
        return uniqueEdges.size();
    }

    // The out degree of the node with the highest out degree.
    public int maxOutDegree() {
        Set<QueryVariable> nodes = this.vertexSet();
        int maxOutDegree = 0;

        for(QueryVariable node : nodes) {
            int outDegree = this.outDegreeOf(node);
            if(outDegree > maxOutDegree) maxOutDegree = outDegree;
        }

        return maxOutDegree;
    }

    // The longest shortest path when the query is considered an undirected graph
    public int diameter() {
        Graph<QueryVariable, DefaultEdge> undirectedQuery = createUndirectedGraph();
        Set<QueryVariable> nodeSet = undirectedQuery.vertexSet();
        List<QueryVariable> nodeList = new ArrayList<>(nodeSet);
        int pathMaxLength = 0;

        for(int i = 0; i < nodeList.size(); i++) {
            for(int j = 0; j < nodeList.size(); j++) {
                QueryVariable node1 = nodeList.get(i);
                QueryVariable node2 = nodeList.get(j);
                DijkstraShortestPath<QueryVariable, DefaultEdge> shortestPath =
                        new DijkstraShortestPath<>(undirectedQuery, node1, node2);
                double pathLengthDouble = shortestPath.getPathLength();
                if(Double.isInfinite(pathLengthDouble)) continue;
                int pathLengthInt = (int)pathLengthDouble;
                if(pathLengthInt > pathMaxLength) pathMaxLength = pathLengthInt;
            }
        }
        return pathMaxLength;
    }

    // Help method for diameter()
    private Graph<QueryVariable, DefaultEdge> createUndirectedGraph() {
        Graph<QueryVariable, DefaultEdge> undirectedQuery = new SimpleGraph<>(DefaultEdge.class);

        Map<String, QueryVariable> newNodes = new HashMap<>();

        Set<QueryVariable> oldNodes = this.vertexSet();
        for(QueryVariable oldNode : oldNodes) {
            String variableName = oldNode.getLabel();
            QueryVariable newNode = new QueryVariable(variableName, "");
            undirectedQuery.addVertex(newNode);
            newNodes.put(variableName, newNode);
        }

        Set<PropertyEdge> oldEdges = this.edgeSet();
        for(PropertyEdge oldEdge : oldEdges) {
            String sourceName = this.getEdgeSource(oldEdge).getLabel();
            String targetName = this.getEdgeTarget(oldEdge).getLabel();
            undirectedQuery.addEdge(newNodes.get(sourceName), newNodes.get(targetName), new DefaultEdge());
        }

        return undirectedQuery;
    }

    // Calculates the shortest distance from focusVar to each unique property in query.
    // Properties with the same name are considered the same.
    public Map<String, Integer> createProximityMap(QueryVariable focusVar) {
        Map<String, Integer> proximityMap = new HashMap<>();
        Set<PropertyEdge> completedEdges = new HashSet<>();
        Set<QueryVariable> currentNodeSet = new HashSet<>();
        Set<QueryVariable> nextNodeSet = new HashSet<>();

        currentNodeSet.add(focusVar);
        int dist = 1;

        // Breath first traversal
        while(!currentNodeSet.isEmpty()) {
            for(QueryVariable var : currentNodeSet) {
                Set<PropertyEdge> outEdges = this.outgoingEdgesOf(var);
                for(PropertyEdge edge : outEdges) {
                    if(completedEdges.contains(edge)) continue;
                    String propertyName = edge.getLabel();
                    if(!proximityMap.containsKey(propertyName)) {
                        proximityMap.put(propertyName, dist);
                    }
                    nextNodeSet.add(this.getEdgeTarget(edge));
                    completedEdges.add(edge);
                }

                Set<PropertyEdge> inEdges = this.incomingEdgesOf(var);
                for(PropertyEdge edge : inEdges) {
                    if(completedEdges.contains(edge)) continue;
                    String propertyName = edge.getLabel();
                    if(!proximityMap.containsKey(propertyName)) {
                        proximityMap.put(propertyName, dist);
                    }
                    nextNodeSet.add(this.getEdgeSource(edge));
                    completedEdges.add(edge);
                }
            }

            currentNodeSet.clear();
            currentNodeSet.addAll(nextNodeSet);
            nextNodeSet.clear();
            dist++;
            
        }
        return proximityMap;
    }
    
}



