package eu.optiquevqs.graph.query;

import java.util.*;


// A query variable
public class QueryVariable {

    private String name;  // The variable name
    protected String type;  // The type (class) of the variable

    // name is any name of the variable. Some kind of id, like "v1".
    // type is the type of the variable. This should ideally match the name of a class or datatype in the navigation graph
    public QueryVariable(String name, String type) {
        this.name = name;
        this.type = type;
    } 

    public String getLabel() {
        return this.name;
    }

    public String getType() {
        return this.type;
    }

    public String toString() {
        return this.name;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) return true;
        if (!(o instanceof QueryVariable)) return false;

        QueryVariable v = (QueryVariable)o;
        if (!v.getLabel().equals(this.name)) return false;
        if (!v.getType().equals(this.type)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.name, this.type);
    }

}