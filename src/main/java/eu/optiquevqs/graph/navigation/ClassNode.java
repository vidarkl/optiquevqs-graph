package eu.optiquevqs.graph.navigation;

// The graphs used in OptiqueVQS can have two types of nodes:
// Datatype nodes, representing an OWL datatype
// Class nodes, representing an OWL class
public class ClassNode extends NavigationGraphNode {

    public ClassNode(String name){
        super(name);
    }
}
