package eu.optiquevqs.graph.navigation;


// A node in the navigation graph. Can either be a datatype or class.
public abstract class NavigationGraphNode {
    private String name;

    public NavigationGraphNode(String name) {
        this.name = name;
    } 

    public String getLabel() {
        return this.name;
    }

    public String toString() {
        return this.name;
    }

}
