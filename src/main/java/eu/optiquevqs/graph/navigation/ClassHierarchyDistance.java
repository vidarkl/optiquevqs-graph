package eu.optiquevqs.graph.navigation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.jgrapht.Graph;
import org.jgrapht.alg.DijkstraShortestPath;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;

import eu.optiquevqs.graph.PropertyEdge;
import eu.optiquevqs.graph.SubClassEdge;

// Calculate shortest distance between classes in the subclass hierarchy for a navigation graph.
public class ClassHierarchyDistance {
	private NavigationGraph navigationGraph;
	private Graph<ClassNode, DefaultEdge> subclassGraph;
	Map<String, Map<String, Integer>> distances;

	public ClassHierarchyDistance(NavigationGraph navigationGraph) {
		this.navigationGraph = navigationGraph;
		this.subclassGraph = this.createSubclassGraph();
		this.distances = this.classHierarchyDistance();
	}

	/* 
	 *  Returns a map of all connected classes with distance.
     *  Distance from non-connected pairs of nodes and distance
     *  from a node to itself is not included.
     */
	public Map<String, Integer> connectedClasses(String classLabel) {
		return this.distances.get(classLabel);
	}

    /*
     *  Calculates shortest distance between pairs of class nodes.
     *  Distance from non-connected pairs of nodes and distance
     *  from a node to itself is not included.
     */
    private Map<String, Map<String, Integer>> classHierarchyDistance() {
    	Map<String, Map<String, Integer>> distances = new HashMap<>();
    	Set<ClassNode> subclassSet = this.subclassGraph.vertexSet();
    	List<ClassNode> subclassList = new ArrayList<>(subclassSet);

    	for(ClassNode node : subclassList) distances.put(node.getLabel(), new HashMap<>());

    	for(int i = 0; i < subclassList.size(); i++) {
    		for(int j = i + 1; j < subclassList.size(); j++) {
    			ClassNode node1 = subclassList.get(i);
    			ClassNode node2 = subclassList.get(j);
    			String nodeLabel1 = node1.getLabel();
    			String nodeLabel2 = node2.getLabel();
    			DijkstraShortestPath<ClassNode, DefaultEdge> shortestPath =
    					new DijkstraShortestPath<>(this.subclassGraph, node1, node2);
    			double pathLength = shortestPath.getPathLength();
    			if(Double.isInfinite(pathLength)) continue;
    			distances.get(nodeLabel1).put(nodeLabel2, (int)pathLength);
    			distances.get(nodeLabel2).put(nodeLabel1, (int)pathLength);
    		}
    	}

    	return distances;
    }

    // Creates an undirected, unlabeled graph of the subclass hierarchy.
    private Graph<ClassNode, DefaultEdge> createSubclassGraph() {
    	Graph<ClassNode, DefaultEdge> subclassGraph = new SimpleGraph<>(DefaultEdge.class);

    	Map<String, ClassNode> newNodes = new HashMap<>();

    	Set<NavigationGraphNode> oldNodes = this.navigationGraph.vertexSet();
    	for(NavigationGraphNode oldNode : oldNodes) {
    		if(!(oldNode instanceof ClassNode)) continue;
    		String classLabel = oldNode.getLabel();
    		ClassNode newNode = new ClassNode(classLabel);
    		subclassGraph.addVertex(newNode);
    		newNodes.put(classLabel, newNode);
    	}

    	Set<PropertyEdge> subclassEdges = this.navigationGraph.edgeSet();
    	for(PropertyEdge subclassEdge : subclassEdges) {
    		if(!(subclassEdge instanceof SubClassEdge)) continue;
    		String sourceLabel = this.navigationGraph.getEdgeSource(subclassEdge).getLabel();
    		String targetLabel = this.navigationGraph.getEdgeTarget(subclassEdge).getLabel();
    		subclassGraph.addEdge(newNodes.get(sourceLabel), newNodes.get(targetLabel), new DefaultEdge());
    	}
    	return subclassGraph;
    }
}
