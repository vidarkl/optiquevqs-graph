package eu.optiquevqs.graph.navigation;

import eu.optiquevqs.graph.*;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.jgrapht.*;
import org.jgrapht.graph.*;

// A navigation graph is a directed multigraph. 
// It contains Classes and Datatypes connected by object properties and datatype properties
// We assume that each edge in the navigation graph represents a unique way of linking two vertices.
// This means that two edges cannot be semantically equivalent, nor inverses.
public class NavigationGraph extends DirectedMultigraph<NavigationGraphNode, PropertyEdge>{

    public NavigationGraph(){
        super(PropertyEdge.class);
    }

    public NavigationGraph(EdgeFactory<NavigationGraphNode, PropertyEdge> ef) {
        super(ef);
    }

    public NavigationGraph(Class<? extends PropertyEdge> edgeClass) {
        this(new ClassBasedEdgeFactory<NavigationGraphNode, PropertyEdge>(edgeClass));
    }

    // Check if an edge is a datatype property
    public boolean isDatatypeProperty(PropertyEdge e){
        return (getEdgeTarget(e) instanceof DatatypeNode);
    }

    // Check if an edge is an object property
    public boolean isObjectProperty(PropertyEdge e){
        return !(e instanceof SubClassEdge) && (getEdgeTarget(e) instanceof ClassNode);
    }

    // Check if an edge is a datatype property
    public boolean isDatatypeProperty(String e) {
    	Set<PropertyEdge> edges = this.edgeSet();
    	for (PropertyEdge edge : edges) {
    		if (edge.getLabel().equals(e)) return isDatatypeProperty(edge);
    	}
    	throw new NoSuchElementException("No edges in the navigation graph with label '" + e + "'.");
    }

    // Check if an edge is an object property
    public boolean isObjectProperty(String e){
    	Set<PropertyEdge> edges = this.edgeSet();
    	for (PropertyEdge edge : edges) {
    		if (edge.getLabel().equals(e)) return isObjectProperty(edge);
    	}
    	throw new NoSuchElementException("No edges in the navigation graph with label '" + e + "'.");
    }

    // Check if edge is an outgoing edge from node
    public boolean isOutgoingEdgeOf(PropertyEdge edge, NavigationGraphNode node) {
    	return outgoingEdgesOf(node).contains(edge);
    }

    // Get NavigationGraphNode given its type. 
    public NavigationGraphNode getNode(String type) {
    	Set<NavigationGraphNode> nodes = this.vertexSet();
    	for (NavigationGraphNode n : nodes) {
    		if (n.getLabel().equals(type)) return n;
    	}
    	throw new NoSuchElementException("No node of type '" + type + "' in the navigation graph.");
    }

    // Get dataproperty edge from node by edge name.
    private PropertyEdge getDatapropertyEdge(NavigationGraphNode node, String datapropertyURI) {
    	Set<PropertyEdge> edges = this.edgeSet();
    	Set<String> superClasses = this.getSuperClasses(node.getLabel());
    	superClasses.add(node.getLabel());
    	for (PropertyEdge e : edges) {
    		for(String superClassLabel : superClasses) {
    			NavigationGraphNode superNode = this.getNode(superClassLabel);
				if (this.isOutgoingEdgeOf(e, superNode) &&
						this.isDatatypeProperty(e) &&
						e.getLabel().equals(datapropertyURI)) {
					return e;
				}
    		}
    	}
    	String msg = "'" + datapropertyURI + "' is not an outgoing data property from '" + node + "' in the navigation graph.";
    	throw new NoSuchElementException(msg);
    }

    // Get the datatype, given string representations of a node and and outgoing edge in the 
    // navigation graph.
    public String getDatatype(String subjectType, String datapropertyURI) {
    	NavigationGraphNode s = this.getNode(subjectType);
    	PropertyEdge p = this.getDatapropertyEdge(s, datapropertyURI);
    	NavigationGraphNode o = this.getEdgeTarget(p);
    	return o.getLabel();
    }

    // Get all datatypes used in navigation graph.
    public Set<String> getAllDatatypes() {
    	Set<String> datatypes = new HashSet<String>();
    	Set<NavigationGraphNode> nodes = this.vertexSet();
    	for (NavigationGraphNode n : nodes) if (n instanceof DatatypeNode) datatypes.add(n.getLabel());
    	return datatypes;
    }

    // Get all classes used in navigation graph.
    public Set<String> getAllClasses() {
    	Set<String> classes = new HashSet<String>();
    	Set<NavigationGraphNode> nodes = this.vertexSet();
    	for (NavigationGraphNode n : nodes) if (n instanceof ClassNode) classes.add(n.getLabel());
    	return classes;
    }

    // Get pairs of <predicate, type> for all outgoing datatype properties from a node with specified type.
    public Set<Pair<String, String>> getOutgoingDatatypePairs(String nodeType) {
    	Set<Pair<String, String>> outgoingDatatypePairs = new HashSet<>();
    	NavigationGraphNode node = this.getNode(nodeType);
    	Set<PropertyEdge> outgoingEdges = this.outgoingEdgesOf(node);
    	for (PropertyEdge e : outgoingEdges) {
    		if (this.isDatatypeProperty(e)) {
    			String edgeLabel = e.getLabel();
    			String targetNodeLabel = this.getEdgeTarget(e).getLabel();
    			outgoingDatatypePairs.add(new ImmutablePair<String, String>(edgeLabel, targetNodeLabel));
    		}
    	}
    	return outgoingDatatypePairs;
    }

    // Get pairs of <predicate, type> for all outgoing object properties from a node with specified type.
    public Set<Pair<String, String>> getOutgoingObjectPairs(String nodeType) {
    	Set<Pair<String, String>> outgoingObjectPairs = new HashSet<>();
    	NavigationGraphNode node = this.getNode(nodeType);
    	Set<PropertyEdge> outgoingEdges = this.outgoingEdgesOf(node);
    	for (PropertyEdge e : outgoingEdges) {
    		if (this.isObjectProperty(e)) {
    			String edgeLabel = e.getLabel();
    			String targetNodeLabel = this.getEdgeTarget(e).getLabel();
    			outgoingObjectPairs.add(new ImmutablePair<String, String>(edgeLabel, targetNodeLabel));
    		}
    	}
    	return outgoingObjectPairs;
    }

    // Get pairs of <predicate, type> for all incoming object properties to a node with specified type.
    public Set<Pair<String, String>> getIncomingObjectPairs(String nodeType) {
    	Set<Pair<String, String>> incomingObjectPairs = new HashSet<>();
    	NavigationGraphNode node = this.getNode(nodeType);
    	Set<PropertyEdge> outgoingEdges = this.incomingEdgesOf(node);
    	for (PropertyEdge e : outgoingEdges) {
    		if (this.isObjectProperty(e)) {
    			String edgeLabel = e.getLabel();
    			String sourceNodeLabel = this.getEdgeSource(e).getLabel();
    			incomingObjectPairs.add(new ImmutablePair<String, String>(edgeLabel, sourceNodeLabel));
    		}
    	}
    	return incomingObjectPairs;
    }

    // Get all subclasses for a node with specified type, including given node.
    public Set<String> getSubClasses(String nodeType) {
    	Set<String> subClasses = new HashSet<>();
    	subClasses.add(nodeType);
    	NavigationGraphNode node = this.getNode(nodeType);
    	Set<PropertyEdge> incomingEdges = this.incomingEdgesOf(node);
    	for (PropertyEdge e : incomingEdges) {
    		if (e instanceof SubClassEdge) {
    			String subClass = this.getEdgeSource(e).getLabel();
    			subClasses.addAll(this.getSubClasses(subClass));
    		}
    	}
    	return subClasses;
    }

    // Get all superclasses for a node with specified type, including given node.
    public Set<String> getSuperClasses(String nodeType) {
    	Set<String> superClasses = new HashSet<>();
    	superClasses.add(nodeType);
    	NavigationGraphNode node = this.getNode(nodeType);
    	Set<PropertyEdge> outgoingEdges = this.outgoingEdgesOf(node);
    	for(PropertyEdge e : outgoingEdges) {
    		if (e instanceof SubClassEdge) {
    			String superClass = this.getEdgeTarget(e).getLabel();
    			superClasses.addAll(this.getSuperClasses(superClass));
    		}
    	}
    	return superClasses;
    }

    // Get all subclasses one level below given node.
    public Set<String> getNearestSubClasses(String nodeType) {
    	Set<String> subClasses = new HashSet<>();
    	NavigationGraphNode node = this.getNode(nodeType);
    	Set<PropertyEdge> incomingEdges = this.incomingEdgesOf(node);
    	for(PropertyEdge e : incomingEdges) {
    		if(e instanceof SubClassEdge) {
    			String subClass = this.getEdgeSource(e).getLabel();
    			subClasses.add(subClass);
    		}
    	}
    	return subClasses;
    }

    // Get all superclasses one level above given node.
    public Set<String> getNearestSuperClasses(String nodeType) {
    	Set<String> subClasses = new HashSet<>();
    	NavigationGraphNode node = this.getNode(nodeType);
    	Set<PropertyEdge> outgoingEdges = this.outgoingEdgesOf(node);
    	for(PropertyEdge e : outgoingEdges) {
    		if(e instanceof SubClassEdge) {
    			String subClass = this.getEdgeTarget(e).getLabel();
    			subClasses.add(subClass);
    		}
    	}
    	return subClasses;
    }

    // Get all subclasses one level below given nodes.
    public Set<String> getNearestSubClasses(Set<String> nodeTypes) {
    	Set<String> subClasses = new HashSet<>();

    	Set<NavigationGraphNode> nodes = new HashSet<>();
    	for(String nodeType : nodeTypes) nodes.add(this.getNode(nodeType));

    	for(NavigationGraphNode node : nodes) {
			Set<PropertyEdge> incomingEdges = this.incomingEdgesOf(node);
			for(PropertyEdge e : incomingEdges) {
				if(e instanceof SubClassEdge) {
					String subClass = this.getEdgeSource(e).getLabel();
					subClasses.add(subClass);
				}
			}
    	}
    	return subClasses;
    }

    // Get all superclasses one level abobve given nodes.
    public Set<String> getNearestSuperClasses(Set<String> nodeTypes) {
    	Set<String> superClasses = new HashSet<>();

    	Set<NavigationGraphNode> nodes = new HashSet<>();
    	for(String nodeType : nodeTypes) nodes.add(this.getNode(nodeType));

    	for(NavigationGraphNode node : nodes) {
			Set<PropertyEdge> outgoingEdges = this.outgoingEdgesOf(node);
			for(PropertyEdge e : outgoingEdges) {
				if(e instanceof SubClassEdge) {
					String superClass = this.getEdgeTarget(e).getLabel();
					superClasses.add(superClass);
				}
			}
    	}
    	return superClasses;
    }

    // Returns top subject class and top object class for given triple.
    public String[] topSuperclasses(String subjectType, String propertyLabel, String objectType, boolean inverted) {
    	if(this.isDatatypeProperty(propertyLabel)) {
    		throw new IllegalArgumentException(propertyLabel + " is not an object property.");
    	}
    	String subjectTopClass = "";
    	String objectTopClass = "";
    	Set<String> subjectSuperClasses = inverted ? this.getSuperClasses(objectType) : this.getSuperClasses(subjectType);
    	Set<String> objectSuperClasses = inverted ? this.getSuperClasses(subjectType) : this.getSuperClasses(objectType);

    	for(PropertyEdge edge : this.edgeSet()) {
    		if(!edge.getLabel().equals(propertyLabel)) continue;
    		String s = this.getEdgeSource(edge).getLabel();
    		String o = this.getEdgeTarget(edge).getLabel();
    		if(subjectSuperClasses.contains(s) && objectSuperClasses.contains(o)) {
    			subjectTopClass = s;
    			objectTopClass = o;
    			return new String[] {subjectTopClass, objectTopClass};
    		}
    	}
    	throw new NoSuchElementException("Top classes not found for <" + subjectType + ", " + propertyLabel + ", " + objectType + ">");
    }

}
