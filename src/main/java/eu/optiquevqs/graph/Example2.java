package eu.optiquevqs.graph;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import eu.optiquevqs.graph.navigation.ClassHierarchyDistance;
import eu.optiquevqs.graph.navigation.ClassNode;
import eu.optiquevqs.graph.navigation.DatatypeNode;
import eu.optiquevqs.graph.navigation.NavigationGraph;
import eu.optiquevqs.graph.navigation.NavigationGraphNode;

public class Example2 {

	public static void main(String[] args) {
        NavigationGraph ng = new NavigationGraph();

        ClassNode mammalClass = new ClassNode("Mammal");
        ClassNode humanClass = new ClassNode("Human");
        ClassNode minorClass = new ClassNode("Minor");
        ClassNode adultClass = new ClassNode("Adult");
        ClassNode vehicleClass = new ClassNode("Vehicle");
        ClassNode carClass = new ClassNode("Car");
        ClassNode electricCarClass = new ClassNode("ElectricCar");

        ng.addVertex(mammalClass);
        ng.addVertex(humanClass);
        ng.addVertex(minorClass);
        ng.addVertex(adultClass);
        ng.addVertex(vehicleClass);
        ng.addVertex(carClass);
        ng.addVertex(electricCarClass);

        DatatypeNode stringDatatype = new DatatypeNode("String");

        ng.addVertex(stringDatatype);

        ng.addEdge(humanClass, carClass, new PropertyEdge("ownsCar"));
        ng.addEdge(vehicleClass, stringDatatype, new PropertyEdge("color"));

        ng.addEdge(adultClass, humanClass, new SubClassEdge());
        ng.addEdge(minorClass, humanClass, new SubClassEdge());
        ng.addEdge(humanClass, mammalClass, new SubClassEdge());
        ng.addEdge(electricCarClass, carClass, new SubClassEdge());
        ng.addEdge(carClass, vehicleClass, new SubClassEdge());

        System.out.println("--- Human ---");

        Set<String> subClasses = ng.getNearestSubClasses("Human");
        Set<String> superClasses = ng.getNearestSuperClasses("Human");
        System.out.println("Subclasses:");
        for(String subClass : subClasses) System.out.println(subClass);
        System.out.println("Superclasses:");
        for(String superClass : superClasses) System.out.println(superClass);

        System.out.println();
        System.out.println("--- Minor ---");

        ClassHierarchyDistance distance = new ClassHierarchyDistance(ng);
        Map<String, Integer> connectedClasses = distance.connectedClasses("Minor");
        for(Entry<String, Integer> entry : connectedClasses.entrySet()) {
        	System.out.println(entry.getKey() + " " + entry.getValue());
        }

        System.out.println();
        System.out.println("done!");
	}

}
