package eu.optiquevqs.graph;

public class SubClassEdge extends PropertyEdge {

	public SubClassEdge() {
		super("subClassOf");
	}

}
